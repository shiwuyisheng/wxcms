<div class="fly-panel" style="margin-bottom: 0;">

    <div class="fly-panel-title fly-filter">
        <a href="" class="layui-this">企业动态</a>
    </div>

    <ul class="fly-list">
    <#if articlelist ? exists>
        <#list articlelist as article>
            <#if article_index lt 15>
            <li>
                <h2>
                    <a class="layui-badge">企业动态</a>
                    <a href="${channel.channel_catalog+article.article_pk+".html"}">${article.article_title}</a>
                </h2>
                <div class="fly-list-badge">
                    <span class="layui-badge layui-bg-black">${article.article_sendtime?substring(0,10)}</span>
                    <!--<span class="layui-badge layui-bg-red">精帖</span>-->
                </div>
            </li>
            </#if>
        </#list>
    </#if>
    </ul>
</div>