<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2018/3/20
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}/static/css/ztree/metro/ztree.css"  media="all">
    <script src="${basePath}/static/plugins/layui/layui.js"></script>
</head>
<body>
<div class="container" style="height:450px;overflow-y:scroll;">

    <form class="layui-form" action="">
        <input id="role_pk" name="role_pk" type="hidden" />
        <ul id="ztree" class="ztree" style="margin-top:0; width:160px;"></ul>
        <div class="layui-form-item">
            <div align="center" class="layui-input-block" style="margin: 5% auto">
                <button class="layui-btn layui-btn-small" align="center" id="edit">确认</button>
                <button class="layui-btn layui-btn-small" id="reset" align="center" type="reset">重置</button>
            </div>
        </div>
    </form>
</div>
<script>
    layui.config({
        base: '${basePath}/static/js/'
    }).use(['ztree', 'layer'], function() {
        var $ = layui.jquery,
            layer = layui.layer;

        var setting = {
            view: {
                addHoverDom: addHoverDom,
                removeHoverDom: removeHoverDom,
                selectedMulti: false
            },
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            edit: {
                enable: true
            },
            callback: {
//                onClick: function(e, treeId, treeNode) {
//                    console.log(treeNode);
//                }
                onCheck:onCheck
            }
        };

        var jsonArray= [];
        function onCheck(e,treeId,treeNode){
            var treeObj=$.fn.zTree.getZTreeObj("ztree"),
//            nodes=treeObj.getCheckedNodes(true)
            nodes=treeObj.getNodes(true);
            jsonArray=[];
            for(var i=0;i<nodes.length;i++){
//            menus_pk+=nodes[i].id + ",";
                jsonArray.push(nodes[i]);
            }
            console.log(JSON.stringify(jsonArray));
        }

        $(document).ready(function() {
            var list_url = "${basePath}/menu/authority_ztree";
            var role_pk=$("#role_pk").val();
            $.ajax({
                type:'post',
                url:list_url,
                data:{role_pk:role_pk},
                success:function (response) {
//                    inittree(response)
                    $.fn.zTree.init($("#ztree"), setting, response);
                }
            })

        });

        var newCount = 1;

        /**
         * 增加节点
         * @param treeId
         * @param treeNode
         */
        function addHoverDom(treeId, treeNode) {
            var sObj = $("#" + treeNode.tId + "_span");
            if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0)
                return;
            var addStr = "<span class='button add' id='addBtn_" + treeNode.tId +
                "' title='add node' onfocus='this.blur();'></span>";
            sObj.after(addStr);
            var btn = $("#addBtn_" + treeNode.tId);
            if (btn) {
                btn.bind("click", function() {
                    var zTree = $.fn.zTree.getZTreeObj("ztree");
                    zTree.addNodes(treeNode, {
                        id: (100 + newCount),
                        pId: treeNode.id,
                        name: "new node" + (newCount++)
                    });
                    return false;
                });
            }
        };

        /**
         * 删除节点
         * @param treeId
         * @param treeNode
         */
        function removeHoverDom(treeId, treeNode) {
            $("#addBtn_" + treeNode.tId).unbind().remove();
        };

        $("#edit").on('click',function(){
            var url="${basePath}/role/saveRole";
            var role_pk=$("#role_pk").val();
            if(jsonArray .length>0){
                var loading = parent.layer.load(1);//弹出加载层
                $.ajax({
                    type : 'post',
                    url : url,
                    data:{
                        datas:JSON.stringify(jsonArray),
                        role_pk:role_pk
                    },
                    success:function(response){
                        parent.layer.alert(response.msg);
                        parent.layer.close(loading);
                        window.location.reload();
                    },
                    error:function (response) {
                        parent.layer.alert(response.msg);
                        parent.layer.close(loading);
                        window.location.reload();
                    }
                });
            }
            else
            {
                layer.alert("请选择权限再保存！");
            }
            return false;
        });
    });
</script>
</body>
</html>
